import sqlite3
import sys

#USED for updates to DB

def updateInspectionRecord(startTime, endTime):
    """Used when exiting the inspection zone to add endtime 

    Arguments:
        startTime {int} -- [startTime of the inspection record that must be updated]
        endTime {int} -- [description]

    Returns:
        [list] -- [last inserted row inspectionRecord table]
    """
    # inspectionRecordId = 3
    # endTime = 1200000000000

    try:
        db = sqlite3.connect('/Users/paulyd/Documents/CATENA/Projects/SmartSnap_RPi/DB/vlak_vark_sqlite_db.db')
        cursor = db.cursor()
        cursor.execute('UPDATE inspectionRecord SET endTime = ? WHERE startTime = ?', (endTime, startTime))
        db.commit()

    except Exception as e:
        db.rollback()
        raise e

    finally: 
        cursor.execute('SELECT * FROM inspectionRecord ORDER BY id DESC LIMIT 1')
        result = cursor.fetchone()       
        db.close()
        return result


def addFeatureMediaItem(data):
    """Used to add a list of features to rows in the DB by filename (from the mathematica kernel)
    
    Arguments:
        data list made up of:
        id {int} -- [id of row to update]
        feature {string} -- [String description of the feature extraction result]
        
    
    Returns:
        [list] --- [last inserted row in mediaItem table]
    """

    #data = [[1,'Feature'],[2,'NOT A TRUCK!'],[3,'TRUCK']]

    try:
        db = sqlite3.connect('/Users/paulyd/Documents/CATENA/Projects/SmartSnap_RPi/DB/vlak_vark_sqlite_db.db')
        cursor = db.cursor()
        for i in range(len(data)):
            cursor.execute('UPDATE mediaItem SET imageFeature = ? WHERE id = ?', (data[i][1], data[i][0]))
        db.commit()

    except Exception as e:
        db.rollback()
        raise e

    finally: 
        cursor.execute('SELECT * FROM mediaItem ORDER BY id DESC LIMIT 1')
        result = cursor.fetchone()       
        db.close()
        return result



def addCompressedLocationMediaItem(data):
    """Used to add a compressed file location path
    
    Arguments:
        data list made up of:
        id {int} -- [id of row to update]
        compressedLocation {string} -- [String location of compressed image copy]
      
    Raeturns:
        [list] -- [returns last inserted row in mediaItem table]
    """

    #data = [[1,'C:/compressedLocation'],[2,'C:/compressedLocation'],[3','C:/compressedLocation']]

    try:
        db = sqlite3.connect('/Users/paulyd/Documents/CATENA/Projects/SmartSnap_RPi/DB/vlak_vark_sqlite_db.db')
        cursor = db.cursor()
        for i in range(len(data)):
            cursor.execute('UPDATE mediaItem SET compressedLocation = ? WHERE id= ?', (data[i][1], data[i][0]))
        db.commit()

    except Exception as e:
        db.rollback()
        raise e

    finally: 
        cursor.execute('SELECT * FROM mediaItem ORDER BY id DESC LIMIT 1')
        result = cursor.fetchone()       
        db.close()
        return result
     


if __name__ == '__main__':
    globals()[sys.argv[1]]()