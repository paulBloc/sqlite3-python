import sqlite3
import sys

### USED FOR INSERTING INTO DB

def createInspectionRecord(inspectionZoneId, startTime, IOTIdentifier):
    """For creating an inspection record on first zone entry
    Arguments:
        inspectionZoneId {int} -- [The zone ID of the entered zone]
        startTime {int} -- [Timestamp on zone entry]
        IOTIdentifier {int} -- [Unique identifier for device]
    Returns:
        [list] -- [last inserted row in inspectionRecord table]
    """

    # inspectionZoneId = 1
    # startTime = 1111123
    # IOTIdentifier = 112
    
    try:
        db = sqlite3.connect('/Users/paulyd/Documents/CATENA/Projects/SmartSnap_RPi/DB/vlak_vark_sqlite_db.db')
        cursor = db.cursor()
        cursor.execute('INSERT INTO inspectionRecord (inspectionZoneId, startTime, IOTIdentifier) VALUES (?,?,?)',
        (inspectionZoneId, startTime, IOTIdentifier))
        db.commit()
    
    except Exception as e:
        db.rollback()
        raise e
        
    finally: 
        cursor.execute('SELECT * FROM inspectionRecord ORDER BY id DESC LIMIT 1')
        result = cursor.fetchone()       
        db.close()
        return result

        
def createMediaItem(inspectionRecordId, timeStamp, latLong, heading, filename, fileLocation):
    """For creating Media Items (images), called each time a capture action has completed
    Arguments:
        inspectionRecordId {int} -- [inspectionRecordId = 1]
        timeStamp {int} -- [timeStamp = 123456789]
        latLong {text} -- [latLong = '28.3456, -23.4567']
        heading {numeric} -- [heading = 233.34]
        filename {text} -- [filename = 'Filename.jpg']
        fileLocation {text} -- [fileLocation = 'C:/images/smartsnap_vlakvark/']
    Returns:
        [list] -- [last inserted row in mediaItem table]
    """

    #inspectionRecordId = 1
    #timeStamp = 123456789
    #latLong = '28.3456, -23.4567'
    #heading = 233.34
    #filename = 'Filename6.jpg'
    #fileLocation = 'C:/images/smartsnap_vlakvark/'

    try:
        db = sqlite3.connect('/Users/paulyd/Documents/CATENA/Projects/SmartSnap_RPi/DB/vlak_vark_sqlite_db.db')
        cursor = db.cursor()
        cursor.execute('INSERT INTO mediaItem (inspectionRecordId, timeStamp, latLong, heading, filename, fileLocation) VALUES (?,?,?,?,?,?)', 
        (inspectionRecordId, timeStamp, latLong, heading, filename, fileLocation))
        db.commit()

    except Exception as e:
        db.rollback()
        raise e

    finally: 
        cursor.execute('SELECT * FROM mediaItem ORDER BY id DESC LIMIT 1')
        result = cursor.fetchone()       
        db.close()
        return result


def createGPSLog(inspectionRecordId, data):
    """Used to write the GPS log to the gpsLog table 
    Arguments:
        inspectionRecord {int} -- [The ID of the inspection record]
        data {list of lists} -- data = [[timeStamp, lat, lng, heading, speed, accuracy, altitude],...]
    Returns:
        [list] -- [last inserted row in gpsLog table]
    """

    # data = [[timeStamp, lat, lng, heading, speed, accuracy, altitude],...] list of lists 
    #data = [[1111,23,23,204.5,23.4,3,125], [1111,23,23,204.5,23.4,3,125], [1111,23,23,204.5,23.4,3,125]]
    #inspectionRecordId = 2

    for i in range(len(data)):
        data[i].insert(0, inspectionRecordId)

    try:
        db = sqlite3.connect('/Users/paulyd/Documents/CATENA/Projects/SmartSnap_RPi/DB/vlak_vark_sqlite_db.db')
        cursor = db.cursor()
        cursor.executemany("""INSERT INTO gpsLog (inspectionRecordId, timeStamp, lat, lng, heading, speed, accuracy, altitude) 
                            VALUES (?,?,?,?,?,?,?,?)""", data)
        db.commit()

    except Exception as e:
        db.rollback()
        raise e

    finally: 
        cursor.execute('SELECT * FROM gpsLog ORDER BY id DESC LIMIT 1')
        result = cursor.fetchone()       
        db.close()
        return result


def createTelemetryLog(inspectionRecordId, data):
    """Used to write the telemetry log to the telemetryLog table 
    Arguments:
        inspectionRecordId {int} -- [Inspection Record ID]
        data {list of lists} -- [[all,the,telemetry,data]]
    Returns:
        [list] -- [last inserted row in telemetryLog table]
    """
    
    # data = [[all,the,telemetry,data]] list of lists 
    #data = [[1111,233,23,23,1.5,1.5,1.5,2.5,2.5,2.5,3.5,3.5,3.5,1,1,1,1,1.6,1.6,1.6,1,1,1,25,1,1,1,1], [1111,233,23,23,1.5,1.5,1.5,2.5,2.5,2.5,3.5,3.5,3.5,1,1,1,1,1.6,1.6,1.6,1,1,1,25,1,1,1,1],[1111,233,23,23,1.5,1.5,1.5,2.5,2.5,2.5,3.5,3.5,3.5,1,1,1,1,1.6,1.6,1.6,1,1,1,25,1,1,1,1]]
    #inspectionRecordId = 1

    for i in range(len(data)):
        data[i].insert(0, inspectionRecordId)

    try:
        db = sqlite3.connect('/Users/paulyd/Documents/CATENA/Projects/SmartSnap_RPi/DB/vlak_vark_sqlite_db.db')
        cursor = db.cursor()
        cursor.executemany("""INSERT INTO telemetryLog (inspectionRecordId, timeStamp, heading, roll, pitch, accX, accY, accZ, gyroX, gyroY, gyroZ,
                                                        magX, magY, magZ, orientX, orientY, orientZ, orientW, linAccX, linAccY, linAccZ,
                                                        gravX, gravY, gravZ, tempC, sys, gyro, accel, mag) 
                            VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)""", data)
        db.commit()

    except Exception as e:
        db.rollback()
        raise e

    finally: 
        cursor.execute('SELECT * FROM telemetryLog ORDER BY id DESC LIMIT 1')
        result = cursor.fetchone()       
        db.close()
        return result

if __name__ == '__main__':
    globals()[sys.argv[1]]()




