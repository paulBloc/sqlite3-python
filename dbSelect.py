import sqlite3
import sys

# USED FOR EXPORTING FROM SQLITE DB (either to send to SQL through API or to use in SmartSnap Logic)


def selectInspectionZone():
    """Used to export InspectionZone details for in-app logic execution, need full list since will loop through all zones to check geofence crossing
    Returns:
        list of lists -- [[all, the, zone details]]
        id: (id is NB for references when records get created, here it is treated as a dataq column)
        captureType:
        name:
        geoFencePoints:
        captureFrequency:
        imageLimit:
        isWifiZone:
        timeLimit
        videoQuality:
        frameRate:
    """
    try:
        db = sqlite3.connect(
            '/Users/paulyd/Documents/CATENA/Projects/SmartSnap_RPi/DB/vlak_vark_sqlite_db.db')
        cursor = db.cursor()
        cursor.execute('SELECT * FROM inspectionZone')
        # fetch rows
        all_rows = cursor.fetchall()
        data = list(all_rows)
        # populate data
        i = 0
        for row in all_rows:
            data[i] = list(row)
            i = i+1

    except Exception as e:
        db.rollback()
        raise e

    finally:
        db.close()
        return data


def selectInspectionRecord(inspectionZoneId):
    """For exporting InspectionRecord rows by inspection zone (these will be inserted into the SQL inspectionRecord table through the API)
    Returns:
        list of inspectionRecords: {list of lists} --- [[ispectionZoneId,startTime,endTime,IOTIdentifier],[ispectionZoneId,startTime,endTime,IOTIdentifier],[,..]]
    """
    #inspectionZoneId = 2
    try:
        db = sqlite3.connect(
            '/Users/paulyd/Documents/CATENA/Projects/SmartSnap_RPi/DB/vlak_vark_sqlite_db.db')
        cursor = db.cursor()
        cursor.execute(
            'SELECT inspectionZoneId, startTime, IOTIdentifier FROM inspectionRecord WHERE inspectionZoneId = ?', (inspectionZoneId,))
        # fetch rows
        all_rows = cursor.fetchall()
        data = list(all_rows)
        # populate data
        i = 0
        for row in all_rows:
            data[i] = list(row)
            i = i+1

    except Exception as e:
        db.rollback()
        raise e

    finally:
        db.close()
        return data


def selectAllInspectionRecords():
    """For exporting InspectionRecord rows by inspection zone (these will be inserted into the SQL inspectionRecord table through the API)
    Returns:
        list of inspectionRecords: {list of lists} --- [[id,ispectionZoneId,startTime,endTime,IOTIdentifier],[id,ispectionZoneId,startTime,endTime,IOTIdentifier],[,..]]
    """
    #inspectionZoneId = 2
    try:
        db = sqlite3.connect(
            '/Users/paulyd/Documents/CATENA/Projects/SmartSnap_RPi/DB/vlak_vark_sqlite_db.db')
        cursor = db.cursor()
        cursor.execute(
            'SELECT id, inspectionZoneId, startTime, IOTIdentifier FROM inspectionRecord')
        # fetch rows
        all_rows = cursor.fetchall()
        data = list(all_rows)
        # populate data
        i = 0
        for row in all_rows:
            data[i] = list(row)
            i = i+1

    except Exception as e:
        db.rollback()
        raise e

    finally:
        db.close()
        return data


def selectMediaItemLocation(inspectionRecordId):
    """Used to export list of id, filelocation and filenames for an inspection record for input to the mathematica kernel 
    Returns:
        list of media item locations: {list of lists} -- [[id, filename, filelocation], [id, filename, filelocation],...]
    """
    #inspectionRecordId = 1
    try:
        db = sqlite3.connect(
            '/Users/paulyd/Documents/CATENA/Projects/SmartSnap_RPi/DB/vlak_vark_sqlite_db.db')
        cursor = db.cursor()
        cursor.execute(
            'SELECT id, filename, fileLocation FROM mediaItem WHERE inspectionRecordId = ?', (inspectionRecordId,))
        # fetch rows
        all_rows = cursor.fetchall()
        data = list(all_rows)
        # populate data
        i = 0
        for row in all_rows:
            data[i] = list(row)
            i = i+1

    except Exception as e:
        db.rollback()
        raise e

    finally:
        db.close()
        return data


def selectMediaItemCompressedLocation(inspectionRecordId):
    """Used to export list of compressedLocation and filenames for an inspection record for synchronising compressed images to blob
    Returns:
        list of compressed locations: {list of lists} -- [[filename, compressedLocation], [filename, compressedLocation],...]
    """
    #inspectionRecordId = 1
    try:
        db = sqlite3.connect(
            '/Users/paulyd/Documents/CATENA/Projects/SmartSnap_RPi/DB/vlak_vark_sqlite_db.db')
        cursor = db.cursor()
        cursor.execute(
            'SELECT filename, compressedLocation FROM mediaItem WHERE inspectionRecordId = ?', (inspectionRecordId,))
        # fetch rows
        all_rows = cursor.fetchall()
        data = list(all_rows)
        # populate data
        i = 0
        for row in all_rows:
            data[i] = list(row)
            i = i+1

    except Exception as e:
        db.rollback()
        raise e

    finally:
        db.close()
        return data


def selectMediaItems(inspectionRecordId):
    """Used to export entire mediaItem table by inspectionRecordId
    Returns:
        list of media items: {list of lists} ---[full table] does not contain id column
    """
    #inspectionRecordId = 1
    try:
        db = sqlite3.connect(
            '/Users/paulyd/Documents/CATENA/Projects/SmartSnap_RPi/DB/vlak_vark_sqlite_db.db')
        cursor = db.cursor()
        cursor.execute(
            'SELECT latLong, timestamp, heading, filename, imageFeature FROM mediaItem WHERE inspectionRecordId = ?', (inspectionRecordId,))
        # fetch rows
        all_rows = cursor.fetchall()
        data = list(all_rows)
        # populate data
        i = 0
        for row in all_rows:
            data[i] = list(row)
            i = i+1
        # remove ID from output
        output = []
        i = 0
        for i in range(len(data)):
            item = data[i]
            slicedItem = item[1:]
            output.append(slicedItem)

    except Exception as e:
        db.rollback()
        raise e

    finally:
        db.close()
        return output


def selectGPSLog(inspectionRecordId):
    """Returns GPS log for supplied inspectionRecordId, no row id returned, just pure data
    Returns:
        gpsLog list: {list of lists} -- [[gps,log,data], [gps,log,data],...]
    """
    #inspectionRecordId = 1
    try:
        db = sqlite3.connect(
            '/Users/paulyd/Documents/CATENA/Projects/SmartSnap_RPi/DB/vlak_vark_sqlite_db.db')
        cursor = db.cursor()
        cursor.execute(
            'SELECT * FROM gpsLog WHERE inspectionRecordId = ?', (inspectionRecordId,))
        # fetch rows
        all_rows = cursor.fetchall()
        data = list(all_rows)
        print(data)
        # populate data
        i = 0
        for row in all_rows:
            data[i] = list(row)
            i = i+1
        # remove ID from output
        output = []
        i = 0
        for i in range(len(data)):
            item = data[i]
            slicedItem = item[1:]
            output.append(slicedItem)
        print(output)

    except Exception as e:
        db.rollback()
        raise e

    finally:
        db.close()
        return output


def selectTelemetryLog(inspectionRecordId):
    """Returns telemetryLog for supplied inspectionRecordId - no row id returned, just pure data
    Returns:
        telemetryLog: {list of lists} -- [[all,the,telemetry,data], [all, the ,telemetry, data]]
   """
    #inspectionRecordId = 2
    try:
        db = sqlite3.connect(
            '/Users/paulyd/Documents/CATENA/Projects/SmartSnap_RPi/DB/vlak_vark_sqlite_db.db')
        cursor = db.cursor()
        cursor.execute(
            'SELECT * FROM telemetryLog WHERE inspectionRecordId = ?', (inspectionRecordId,))
        # fetch rows
        all_rows = cursor.fetchall()
        data = list(all_rows)
        # populate data
        i = 0
        for row in all_rows:
            data[i] = list(row)
            i = i+1
        # remove ID from output
        output = []
        i = 0
        for i in range(len(data)):
            item = data[i]
            slicedItem = item[1:]
            output.append(slicedItem)

    except Exception as e:
        db.rollback()
        raise e

    finally:
        db.close()
        return output


if __name__ == '__main__':
    globals()[sys.argv[1]]()
