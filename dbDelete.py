import sqlite3
import sys

# USED for deletes from DB


def removeMediaItem(id):
    """Used to remove records from media item table
    Arguments:
        data {[list]} -- [List of ids to remove]
    Returns:
        [boolean] -- [true or false]
    """
    #id = [1,2,3,4,23]

    try:
        db = sqlite3.connect(
            '/Users/paulyd/Documents/CATENA/Projects/SmartSnap_RPi/DB/vlak_vark_sqlite_db.db')
        cursor = db.cursor()
        for i in range(len(id)):
            cursor.execute('DELETE FROM mediaItem WHERE id = ?', (id[i],))
        db.commit()
        status = True

    except Exception as e:
        db.rollback()
        status = False
        raise e

    finally:
        db.close()
        return status


def removeRecordItem(id):
    """Used to remove records from inspectionRecord table
    Arguments:
        data {[list]} -- [List of ids to remove]
    Returns:
        [boolean] -- [true or false]
    """
    #id = [1,2,3,4,23]

    try:
        db = sqlite3.connect(
            '/Users/paulyd/Documents/CATENA/Projects/SmartSnap_RPi/DB/vlak_vark_sqlite_db.db')
        cursor = db.cursor()
        for i in range(len(id)):
            cursor.execute(
                'DELETE FROM inspectionRecord WHERE id = ?', (id[i],))
        db.commit()
        status = True

    except Exception as e:
        db.rollback()
        status = False
        raise e

    finally:
        db.close()
        return status


if __name__ == '__main__':
    globals()[sys.argv[1]]()


def deleteSingleMediaItem(filename):

    #data = ['Filename1.jpg', 'Filename3.jpg', 'Filename4.jpg']

    try:
        db = sqlite3.connect(
            '/Users/paulyd/Documents/CATENA/Projects/SmartSnap_RPi/DB/vlak_vark_sqlite_db.db')
        cursor = db.cursor()

        cursor.execute(
            'DELETE FROM mediaItem WHERE filename = ?', (filename,))
        db.commit()
        status = True

    except Exception as e:
        db.rollback()
        status = False
        raise e

    finally:
        db.close()
        return status
