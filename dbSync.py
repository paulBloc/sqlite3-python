import sqlite3
import sys

#USED for synchronising tables from SQL

def syncInspectionZone(data):
    """Drop inspectionZone table, create a new one and insert entire table using values extracted from passed list of dicts
    Arguments: 
        {list of dicts} - [the entire InspectionZone table from SQL, must be passed as a list of dicts]
    Returns:
        [list] -- [last inserted row in InspectionZone table]
    """
    #example data
    # data = [{
    #     "id": 1,
    #     "captureType": "FIXED_IMAGE",
    #     "name": "Test Zone 1",
    #     "geoFencePoints": "23,23,45,56,57,67,67,67,67,67,67,67,67,67,67,67",
    #     "captureFrequency": 10,
    #     "imageLimit": 10,
    #     "isWifiZone": 0,
    #     "timeLimit": 300,
    #     "videoQuality": "1080p",
    #     "frameRate": 60
    # },
    # {
    #     "id": 2,
    #     "captureType": "FIXED_IMAGE",
    #     "name": "Test Zone 1",
    #     "geoFencePoints": "23,23,45,56,57,67,67,67,67,67,67,67,67,67,67,67",
    #     "captureFrequency": 10,
    #     "imageLimit": 10,
    #     "isWifiZone": 0,
    #     "timeLimit": 300,
    #     "videoQuality": "1080p",
    #     "frameRate": 60
    # },
    # {
    #     "id": 3,
    #     "captureType": "FIXED_IMAGE",
    #     "name": "Test Zone 1",
    #     "geoFencePoints": "23,23,45,56,57,67,67,67,67,67,67,67,67,67,67,67",
    #     "captureFrequency": 10,
    #     "imageLimit": 10,
    #     "isWifiZone": 0,
    #     "timeLimit": 300,
    #     "videoQuality": "1080p",
    #     "frameRate": 60
    # }]

    #extract values from list of dicts for insert into SQLite inspectionZone table
    insertData = []
    for i in range(len(data)):
        row = list(data[i].values())
        insertData.append(row)
    
    #print(insertData)

    try:
        db = sqlite3.connect('/Users/paulyd/Documents/CATENA/Projects/SmartSnap_RPi/DB/vlak_vark_sqlite_db.db')
        cursor = db.cursor()
        cursor.execute("""DROP TABLE IF EXISTS inspectionZone""")
        cursor.execute("""
        CREATE TABLE IF NOT EXISTS inspectionZone (
                        id INTEGER NOT NULL PRIMARY KEY,
                        captureType TEXT,
                        name TEXT,
                        geoFencePoints TEXT,
                        captureFrequency INTEGER,
                        imageLimit INTEGER,
                        isWifiZone INTEGER,
                        timeLimit INTEGER,
                        videoQuality TEXT,
                        frameRate INTEGER
                        );""")
        cursor.executemany("""
        INSERT INTO inspectionZone (id, captureType, name, geoFencePoints, captureFrequency, imageLimit, isWifiZone, timeLimit, videoQuality, frameRate)
        VALUES (?,?,?,?,?,?,?,?,?,?)""", insertData)

        db.commit()

    except Exception as e:
        db.rollback()
        raise e

    finally: 
        cursor.execute('SELECT * FROM inspectionZone ORDER BY id DESC LIMIT 1')
        result = cursor.fetchone()       
        db.close()
        return result

if __name__ == '__main__':
    globals()[sys.argv[1]]()
